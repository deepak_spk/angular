import { TestBed, inject } from '@angular/core/testing';

import { ProcessHTTPMessageService } from './process-httpmessage.service';

describe('ProcessHTTPMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProcessHTTPMessageService]
    });
  });

  it('should be created', inject([ProcessHTTPMessageService], (service: ProcessHTTPMessageService) => {
    expect(service).toBeTruthy();
  }));
});
