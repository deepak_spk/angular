import { Injectable } from '@angular/core';
import { Promotion } from '../shared/promotion';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMessageService } from '../services/process-httpmessage.service';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { catchError, map } from 'rxjs/operators';



@Injectable()
export class PromotionService {

  constructor(private restangular: Restangular,
    private processHTTPMessageService: ProcessHTTPMessageService) { }

  getPromotions(): Observable<Promotion[]> {
    return this.restangular.all('promotions').getList();
  }

  getPromotion(id: number): Observable<Promotion> {
    return this.restangular.one('promotions', id).get();
  }

  getFeaturedPromotion(): Observable<Promotion> {
    return this.restangular.all('promotions').getList({ featured: true }).pipe(
      map(promotions => promotions[0]));
  }
}
