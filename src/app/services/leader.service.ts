import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMessageService } from '../services/process-httpmessage.service';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { catchError, map } from 'rxjs/operators';



@Injectable()
export class LeaderService {

  constructor(private restangular: Restangular,
    private processHTTPMessageService: ProcessHTTPMessageService) { }

  getLeaders(): Observable<Leader[]> {
    return this.restangular.all('leaders').getList();
  }

  getLeader(id: number): Observable<Leader> {
    return this.restangular.one('leaders', id).get();
  }

  getFeaturedLeader(): Observable<Leader> {
    return this.restangular.all('leaders').getList({ featured: true }).pipe(
      map(promotions => promotions[0]));
  }

}
