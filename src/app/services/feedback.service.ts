import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';
import { ProcessHTTPMessageService } from '../services/process-httpmessage.service';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { Feedback } from '../shared/feedback';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private restangular: Restangular,
    private processHTTPMessageService: ProcessHTTPMessageService) { }

  submitFeedback(feedback: Feedback): Observable<Feedback> {

    return this.restangular.all('feedback').post(feedback);

  }
}
